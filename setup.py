# coding: utf-8

import sys
from setuptools import setup, find_packages

NAME = "pyant"
VERSION = "1.0.0"

# To install the library, run the following
#
# python setup.py install
#
# prerequisite: setuptools
# http://pypi.python.org/pypi/setuptools

REQUIRES = ["connexion"]

setup(
    name=NAME,
    version=VERSION,
    description="Light Cycle REST (like) API",
    author_email="",
    url="",
    keywords=["Swagger", "Light Cycle REST (like) API"],
    install_requires=REQUIRES,
    packages=find_packages(),
    package_data={'': ['swagger/swagger.yaml']},
    include_package_data=True,
    entry_points={
        'console_scripts': ['pyant=pyant.__main__:main']},
    long_description="""\
    This is a REST (like) API for a Light Cycle like game.
    """
)

