import connexion
import flask
import six

from pyant.models.arena import Arena
from pyant.models.bike import Bike
from pyant.repos.game_repo import GameRepo
from pyant import util


game_repo = GameRepo(1)
url = 'localhost:5432'


def create_bike(arenaId):
    """Create a bike in an arena

    Create a bike in an arena. This bike should be only used by the consumer, which created it # noqa: E501

    :param arenaId: ID of the arena to create a bike
    :type arenaId: int

    :rtype: str
    """
    game = game_repo.get(arenaId)

    if game is None:
        return flask.abort(404)

    bike = game.create_bike()

    if bike is False:
        return flask.abort(503)

    return bike


def get_arena(arenaId):
    """Get arena

   

    :param arenaId: ID of the arena to return
    :type arenaId: int

    :rtype: Arena
    """
    game = game_repo.get(arenaId)

    if game is None:
        return flask.abort(404)

    return game.to_arena()


def get_free_arenas():
    """Get multiple free arenas

    Get arenas. The arenas must not be full.  The service should return 503 if no arena is empty. But it might instead return an empty array and 200. The provider optionaly replace  the the uri authority with some server (also it self), which forwards the request to the provider. # noqa: E501


    :rtype: List[str]
    """
    arena_id = game_repo.get_free_game()

    if arena_id is None:
        return flask.abort(503)

    return ['http://{}/arenas/{}'.format(url, arena_id)]


def update_bike(arenaId, bikeColor, body):
    """Updates bike

    Updates bike object # noqa: E501

    :param arenaId: ID of bikes arena
    :type arenaId: int
    :param bikeColor: bike color as hex value
    :type bikeColor: str
    :param body: 
    :type body: dict | bytes

    :rtype: None
    """
    if connexion.request.is_json:
        body = Bike.from_dict(connexion.request.get_json())

    game = game_repo.get(arenaId)

    if game is None:
        return flask.abort(404)

    game.update_bike(bikeColor, body.moving_direction)
