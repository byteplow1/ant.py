#!/usr/bin/env python3

import connexion

from pyant import encoder


def main():
    app = connexion.App(__name__, specification_dir='./swagger/')
    app.app.json_encoder = encoder.JSONEncoder
    app.add_api('swagger.yaml', arguments={'title': 'Light Cycle REST (like) API'})
    app.run(port=5432)


if __name__ == '__main__':
    main()
