from pyant.models import Wall
from pyant.models import Coordinate
from threading import Lock


def _check_direction(direction: str):
    allowed_values = ["north", "east", "south", "west"]
    if direction not in allowed_values:
        raise ValueError(
            "Invalid value for `moving_direction` ({0}), must be one of {1}".format(direction, allowed_values)
        )


class Bike:
    def __init__(self, color: str, start_coordinate: Coordinate, move_direction: str):
        _check_direction(move_direction)

        self.lock = Lock()

        self.wall = Wall(color, [start_coordinate])
        self.moving_direction = move_direction

    def turn(self, moving_direction):
        _check_direction(moving_direction)

        with self.lock:
            if (moving_direction in ['north', 'south'] and self.moving_direction in ['east', 'west']) \
                    or (moving_direction in ['east', 'west'] and self.moving_direction in ['north', 'south']):

                self.moving_direction = moving_direction

    def move(self):
        with self.lock:
            head = self.wall.coordinates[0]

            if self.moving_direction == 'north':
                next_coordinate = Coordinate(head.x, head.y - 1)
            elif self.moving_direction == 'east':
                next_coordinate = Coordinate(head.x + 1, head.y)
            elif self.moving_direction == 'south':
                next_coordinate = Coordinate(head.x, head.y + 1)
            elif self.moving_direction == 'west':
                next_coordinate = Coordinate(head.x - 1, head.y)
            else:
                raise Exception('Invalid state, `self.moving_direction` must be one of "north", "east", "south", "west"')

            self.wall.coordinates.insert(0, next_coordinate)
