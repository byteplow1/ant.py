from typing import Tuple
from threading import Lock
from loopytimer import LoopyTimer

from pyant.game.Bike import Bike
from pyant.models import Coordinate, Arena


def _check_size(size: Tuple[int, int]):
    if size[0] < 1 or size[1] < 1:
        raise ValueError("Invalid value for `size` ({0}), must be positive".format(size))


class Game:
    def __init__(self, size: Tuple[int, int], max_player_count=4):
        self.bikes_lock = Lock()
        self.size = size
        self.bikes = {}
        self.max_player_count = max_player_count
        self.is_started = False
        self.over = False
        self.timer = LoopyTimer(1.0, self._on_tick)

    def is_full(self):
        return self.is_started

    def is_over(self):
        return self.over

    def create_bike(self):
        if len(self.bikes) >= self.max_player_count:
            return False

        with self.bikes_lock:
            color, coordinate, direction = self._create_bike_spawn()

            self.bikes[color] = Bike(color, coordinate, direction)

        self.start()

        return color

    def update_bike(self, color, direction):
        bike = self.bikes.get(color)
        if bike is None:
            return

        bike.turn(direction)
        return

    def start(self):
        if len(self.bikes) < self.max_player_count:
            return
        self.is_started = True
        self.timer.start()

    def to_arena(self):
        walls = []
        for idx, bike in self.bikes.items():
            walls.append(bike.wall)

        return Arena(self.is_started, walls)

    def _create_bike_spawn(self):
        player_count = len(self.bikes) + 1

        if player_count == 1:
            return "111111", Coordinate(2, 2), "south"

        if player_count == 2:
            return "222222", Coordinate(self.size[0] - 3, self.size[1] - 3), "north"

        if player_count == 3:
            return "333333", Coordinate(self.size[0] - 3, 2), "south"

        if player_count == 4:
            return "444444", Coordinate(1, self.size[1] - 3), "north"

    def _on_tick(self):
        with self.bikes_lock:
            for idx, bike in self.bikes.items():
                bike.move()

            deads = set()
            for idx, bike in self.bikes.items():
                for coordinate in bike.wall.coordinates:
                    if coordinate.x > self.size[0] - 1 or coordinate.y > self.size[1] - 1 or coordinate.x < 0 or coordinate.y < 0:
                        deads.add(idx)

                    for another_idx, another_bike in self.bikes.items():
                        if idx == another_idx:
                            continue

                        if coordinate in another_bike.wall.coordinates:
                            if bike.wall.coordinates[0] == coordinate:
                                deads.add(idx)

                            if another_bike.wall.coordinates[0] == coordinate:
                                deads.add(another_idx)

            for dead in deads:
                self.bikes.pop(dead)

            if len(self.bikes) < 2:
                self.timer.cancel()
                self.over = True
