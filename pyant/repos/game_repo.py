from pyant.game.Game import Game
from uuid import uuid1


class GameRepo:
    def __init__(self, max_games):
        self.games = {}
        self.max_games = max_games

    def get_free_game(self):
        for idx, game in self.games.items():
            if not game.is_full():
                return idx

        if len(self.games) <= self.max_games:
            uuid = uuid1().int
            self.games[uuid] = Game((40, 40), 2)

            return uuid

        old = []
        for idx, game in enumerate(self.games):
            if game.is_over():
                return old.append(game)

        for game in old:
            self.games.pop(game)

        if len(self.games) <= self.max_games:
            uuid = uuid1().int
            self.games[uuid] = Game((40, 40), 2)

            return uuid

        return None

    def get(self, idx):
        game = self.games.get(idx)
        return game
