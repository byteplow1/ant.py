# coding: utf-8

from __future__ import absolute_import

from flask import json
from six import BytesIO

from pyant.models.arena import Arena  # noqa: E501
from pyant.models.bike import Bike  # noqa: E501
from pyant.test import BaseTestCase


class TestArenaController(BaseTestCase):
    """ArenaController integration test stubs"""

    def test_create_bike(self):
        """Test case for create_bike

        Create a bike in an arena
        """
        response = self.client.open(
            '//arenas/{arenaId}/bikes'.format(arenaId=56),
            method='POST')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_get_arena(self):
        """Test case for get_arena

        Get arena
        """
        response = self.client.open(
            '//arenas/{arenaId}'.format(arenaId=789),
            method='GET')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_get_free_arenas(self):
        """Test case for get_free_arenas

        Get multiple free arenas
        """
        response = self.client.open(
            '//arenas',
            method='GET')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_update_bike(self):
        """Test case for update_bike

        Updates bike
        """
        body = Bike()
        response = self.client.open(
            '//arenas/{arenaId}/bikes/{bikeColor}'.format(arenaId=56, bikeColor='bikeColor_example'),
            method='PUT',
            data=json.dumps(body),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))


if __name__ == '__main__':
    import unittest
    unittest.main()
